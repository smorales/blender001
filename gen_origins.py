#!/usr/bin/env python
import bpy
import numpy
import os

class SceneGenerator():
    """ Generar escena, init_path indica directorio inicial.
    TODO:
    * sacar numpy
    * profit
    """

    def __init__(self, init_path=os.getcwd(), objets_radius=2.0, init_theta=0.0,
                 custom_delta=1.0, init_origin=numpy.array([0.0, 0.0]),
                 with_reset=False, use_cilinder=False):
        self.__RADIUS=objets_radius
        self.__THETA=init_theta
        self.__FIRST_IN_RING=init_origin
        self.__OBJECTS_COUNT=0
        self.__DELTA=custom_delta
        self.__POL_COORD=numpy.array(init_origin)
        self.__ORIGIN=init_origin
        self.__DIRMODEL="DIRMODEL"
        self.__FILEMODEL="FILEMODEL"
        self.URLPATH=init_path
        self.WITH_RESET=with_reset
        self.USE_CILINDER=use_cilinder

    def debug(self,i=numpy.array([0.0, 0.0])):
        print(i.tolist())

    def calc_theta(self, radius):
        """
        calcula el angulo en que debe colocarse el siguiente objeto
        de radio __RADIUS, en el arco de radio radius
        """
        minimum = 2*self.__RADIUS + self.__DELTA
        temp = numpy.square(minimum) - 2*numpy.square(radius)
        temp = -1*temp/(2*numpy.square(radius))
        return numpy.arccos(temp)

    def calc_square_dist(self,coord,origin=numpy.array([0.0,0.0])):
        temp=numpy.square(coord[0]) + numpy.square(origin[0])
        return numpy.absolute( temp - 2*coord[0]*origin[0]*numpy.cos(coord[1] - origin[1]) )


    def to_escene(self,coord,height,name,model):
        """
        transforma polares en cartesianas y fixea considerando los origenes.
        instancia objecto "model" con altura "height" y nombre "name"
        """
        xy = numpy.array([coord[0] * numpy.cos(coord[1]), coord[0] * numpy.sin(coord[1]) ])
        xy_origin = numpy.array([self.__ORIGIN[0] * numpy.cos(self.__ORIGIN[1]), self.__ORIGIN[0] * numpy.sin(self.__ORIGIN[1]) ])
        xy = xy + xy_origin
        if self.USE_CILINDER:
            bpy.ops.mesh.primitive_cylinder_add(vertices=32, radius=self.__RADIUS, depth=2, end_fill_type='NGON', view_align=False, enter_editmode=False, location=tuple(xy.tolist() + [height]), rotation=(0, 0, 0),)
            bpy.ops.transform.resize(value=(1,1,height))
            ob=bpy.context.object
            ob.name=name
            ob.show_name=False
            ob.select = False
            return
        if model is "directory":
            new_object = bpy.context.scene.objects[self.__DIRMODEL].copy()
        if model is "file":
            new_object = bpy.context.scene.objects[self.__FILEMODEL].copy()
        new_object.name = name
        new_object.dimensions = (3,3,height)
        new_object.location = tuple(xy.tolist() + [1])
        new_object.hide = False
        bpy.context.scene.objects.link(new_object)
        return


    def put_in_scene(self,height, name, model="cilinder",debugme=False):
        """
        cambia el radio de los arcos
        instancia utilizando to_escene()
        """
        if self.__OBJECTS_COUNT==0:
            if debugme: debug(self.__POL_COORD)
            self.__OBJECTS_COUNT += 1
            self.to_escene(self.__POL_COORD,2*height,name,model)
            return
        if self.__OBJECTS_COUNT==1:
            self.__POL_COORD = self.__POL_COORD + numpy.array([2*self.__RADIUS + self.__DELTA, 0])
            self.__FIRST_IN_RING=self.__POL_COORD
            if debugme: debug(self.__POL_COORD)
            self.__OBJECTS_COUNT += 1
            self.to_escene(self.__POL_COORD,height,name,model)
            return

        self.__POL_COORD = self.__POL_COORD + numpy.array([0,self.calc_theta(self.__POL_COORD[0])])

        if self.__POL_COORD[1] > 2*numpy.pi: self.__POL_COORD[1] -= 2*numpy.pi

        if self.calc_square_dist(self.__POL_COORD,self.__FIRST_IN_RING) < numpy.square(2*self.__RADIUS + self.__DELTA):
            self.__POL_COORD = self.__POL_COORD + numpy.array([2*self.__RADIUS + self.__DELTA,0])
            self.__FIRST_IN_RING = self.__POL_COORD

        if debugme: debug(self.__POL_COORD)
        self.__OBJECTS_COUNT += 1
        self.to_escene(self.__POL_COORD,height,name,model)
        return


    def reset_origin(self):
        """
        Mueve el origen a 2 * radio del arco actual
        Resetea los arcos y cantidad de objectos
        """
        self.__ORIGIN = self.__ORIGIN + numpy.array([2*self.__POL_COORD[0] + self.__DELTA,0])
        self.__FIRST_IN_RING = self.__ORIGIN
        self.__POL_COORD = numpy.array([0,0])
        self.__OBJECTS_COUNT = 0



    def run(self):
        #model:
        # tam: 5,5,3
        # linea 57
        for path, dirs, files in os.walk(self.URLPATH):
            self.put_in_scene(5,path,"directory")
            for dirname in dirs:
                self.put_in_scene(5,dirname,"directory")
            for filename in files:
                self.put_in_scene(3,filename,"file")
            if self.WITH_RESET: self.reset_origin()

# init_path, objets_radius=2.0, init_theta=0.0,
# custom_delta=1.0, init_origin=numpy.array([0.0, 0.0]),
# with_reset=False):

abril = SceneGenerator(init_path="/home/smorales/Documents",with_reset=False,use_cilinder=True)
abril.run()
